
class EventCalendar {
	constructor(){
		this.events = [];
		this.timerCounter = 0;
		var addToStorage = this.addToStorage.bind(this);
		window.onbeforeunload = addToStorage;
		//this.getEventsFromStorage();
	}


	getEventsFromStorage(){
		var now = new Date();
		var startIndex = -1;
		if(window.localStorage.getItem('events') != undefined){
			this.events = JSON.parse(window.localStorage.getItem('events'));
			for(let i = 0; i<this.events.length; i++){
				this.events[i].date = new Date(this.events[i].date);
				if(this.events[i].date >= now){
					startIndex = i;
					break;
				}
			}
			if(startIndex >= 0){
				for(let i = startIndex; i<this.events.length; i++){
					let timeLeft = this.events[i].date - now;
					this.setEventTimer(timeLeft, this.events[i].evFunction);
					this.timerCounter +=1;
					this.events[i].timerId = this.timerCounter;
				}
			}
		}

		//setEventTimer(time, callback)
		
	}

	addToStorage(){
		function compareDate(objA, objB){
			return objA.date-objB.date;
		}
		this.events.sort(compareDate);
		/*for(let i = 0; i < this.events.length; i++){
			this.events[i].date = this.events[i].date.getTime();
		}*/
		window.localStorage.setItem('events', JSON.stringify(this.events));
	}

	getAllEvents(){
		return this.events;
	}

	addEvent(evName, dateTime, callback){
		var now = new Date();
		var timeLeft;
		var selectedDate = new Date(dateTime).getTime();
		var ind = this.events.length;
		var evId = (ind > 0) ? this.events[ind-1].id+1 : 0;
		var ev = {
			id: evId,
			name: evName,
			date: selectedDate,
			timerId: 0,
			evFunction: callback.toString()
		}
		this.events.push(ev);
		console.log(ev);
		if(this.events[ind].date >= now){
			timeLeft = this.events[ind].date - now;
			this.timerCounter +=1;
			this.events[ind].timerId = this.timerCounter;
			this.setEventTimer(timeLeft, this.events[ind].evFunction);
		}
		
	}

	setEventTimer(time, callback){
		setTimeout(()=>{
			eval('(' + callback + ')()');
		}, time);
		
	}

	removeEvent(evName){
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		console.log(ind);
		clearTimeout(this.events[ind].timerId);
		(ind>=0)&&this.events.splice(ind, 1);
		
	}

	changeEvent(evName, newName = null, newDate = null, newCallback = null){
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		var now = new Date();
		this.events[ind].name = newName ? newName : this.events[ind].name;
		this.events[ind].date = newDate ? new Date(newDate).getTime() : this.events[ind].date;
		this.events[ind].evFunction = newCallback.toString() ? newCallback.toString() : this.events[ind].evFunction;
		clearTimeout(this.events[ind].timerId);
		if(this.events[ind].date >= now){
			this.timerCounter +=1;
			this.events[ind].timerId = this.timerCounter;
			var timeLeft = this.events[ind].date - now;
			this.setEventTimer(timeLeft, this.events[ind].evFunction);
		}else{
			this.events[ind].timerId = 0;
		}
		
	}

	getWeek(date) {  
		const daysQuantity = 7;
		const thursdayNumber = 4; 
		const weekInMs = 604800000;
	    var target = date;  
	    var dayNr  = (date.getDay() + 6) % daysQuantity;  
	    target.setDate(target.getDate() - dayNr + 3);  
	    var firstThursday = target.valueOf();  
	    target.setMonth(0, 1);  
	    if (target.getDay() != thursdayNumber) {  
	        target.setMonth(0, 1 + ((thursdayNumber - target.getDay()) + daysQuantity) % daysQuantity);  
	    }  
	    return 1 + Math.ceil((firstThursday - target) / weekInMs);
	}

	getEventsByDate(year, month, day){
			var evs = [];
			var endOfDate = 24*3600*1000;
			var sdate = new Date(year, month, day).getTime();
			evs = this.events.filter(ev=>{
				let evDate = ev.date;
				if((evDate >= sdate) && (evDate < (sdate+endOfDate))){
					return ev;
				}
			});
		return evs;
	}

	 getEventsByWeekNumber(weekNumber){
	 	var evs = [];
	 	evs  = this.events.filter(ev=>{
	 		let evDate = new Date(ev.date);
	 		let evWeek = this.getWeek(evDate);
	 		if(evWeek == weekNumber){
	 			return ev;
	 		}
	 	});
	 	return evs;
	 }

	getEventsByMonthNumber(year, month){
			var evs = [];
			evs = this.events.filter(ev=>{
			let evDate = new Date(ev.date).getMonth();
			let reqMon = new Date(year, month).getMonth();
			if(evDate == month){
				return ev;
			}
		});
		return evs;
	}

	getEventsByInterval(startYear, startMonth, startDay, endYear, endMonth, endDay){
		var evs = [];
		var endOfDate = 24*3600*1000;
		var startDate = new Date(startYear, startMonth, startDay).getTime();
		var endDate = new Date(endYear, endMonth, endDay).getTime();
		evs = this.events.filter(ev=>{
			let evDate = ev.date;
			if((evDate >= startDate) && (evDate < (endDate+endOfDate))){
				return ev;
				}
		});
		return evs;
	}

}


class ExtendedEventCalendar extends EventCalendar{
	constructor(commonFunction){
		super();
		this.events = [];
		this.timerCounter = 0;
		this.intervalCounter = 0;
		this.warnTimerCounter = 0;
		this.weeklyCounter = 0;
		this.commonFunction = commonFunction;
		var addToStorage = this.addToStorage.bind(this);
		window.onbeforeunload = addToStorage;
		//this.getEventsFromStorage();
	}

	addRepeatEvent(evName, dateTime, callback){
		const oneDayInMs = 24*3600*1000;
		const daysInWeek = 7;
		//addEvent(evName, dateTime, callback)
		var ind = this.events.length;
		super.addEvent(evName, dateTime, callback);
		var dayNumber = this.events[ind].date.getDay();
		var timeToFirstCall;
		var today = new Date().getDay();
		var daysDifference = today - dayNumber;
		if(daysDifference >= 0){
			timeToFirstCall = daysDifference*oneDayInMs;
		}else{
			timeToFirstCall = (daysInWeek - daysDifference)*oneDayInMs;
		}
		var callbackFunc = callback.toString();
		this.events[ind].repeatDay = dayNumber;
		this.events[ind].repeatFunc = callback.toString();
		this.setEventInterval(timeToFirstCall, callbackFunc);
		this.intervalCounter = +1;
		this.weeklyCounter = +1;
		this.events[ind].intervaId = this.intervalCounter;
		this.events[ind].weekTimerId = this.weeklyCounter;

	}
	addToStorage(){
		super.addToStorage();
	}
	
	/*removeEvent(evName){
	changeEvent(evName, newName = null, newDate = null, newCallback = null){*/

	/*changeEvent(evName, newName = null, newDate = null, newCallback = null){
		super.changeEvent(evName, newName = null, newDate = null, newCallback = null);
	}*/
	removeEvent(evName){
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		if(this.events[ind].hasOwnProperty('weekTimerId')){
			clearTimeout(this.events[ind].weekTimerId);
			clearInterval(this.events[ind].intervaId);
		}
		if(this.events[ind].hasOwnProperty('weekTimerId')){
			clearTimeout(this.events[ind].weekTimerId)
		}
		super.removeEvent(evName);
		
	}

	getEventsFromStorage(){
		const oneDayInMs = 24*3600*1000;
		const daysInWeek = 7;
		var now = new Date();
		var startIndex = 0;
		var today = now.getDay();
		var daysDifference;
		var timeToFirstCall;
		if(window.localStorage.getItem('events') != undefined){
			super.getEventsFromStorage();
			this.events = JSON.parse(window.localStorage.getItem('events'));
			for(let i = 0; i < this.events.length; i++){
				if(!this.events[i].hasOwnProperty('repeatDay')){
					continue;
				}
				daysDifference = today - this.events[i].date.getDay();
				if(daysDifference >= 0){
					timeToFirstCall = daysDifference*oneDayInMs;
				}else{
					timeToFirstCall = (daysInWeek - daysDifference)*oneDayInMs;
				}
				this.setEventInterval(timeToFirstCall, this.events[i].repeatFunc);
			}
			for(let i = 0; i < this.events.length; i++){
				if(this.events[i].hasOwnProperty('timeToWarn')){
					if((this.events[i].date.getTime() - this.events[i].timeToWarn) > (this.events[i].date - now)){
						this.setEventWarn(this.events[i].name, this.events[i].timeToWarn, this.events[i].warnFunc);
					}
				}
			}
		}

	}

	/*addToStorage(){
		super.addToStorage();
	}*/


	setEventInterval(timeToFirstEvCall, callback){
		const weekInMs = 604800000;
		setTimeout(()=>{
			eval('(' + callback + ')()');
		}, timeToFirstEvCall);
		setInterval(()=>{
			eval('(' + callback + ')()');
		}, weekInMs)
	}

	setEventWarn(evName, timeToWarn, callback){
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		var callbackFunc = callback.toString() || commonFunction.toString();
		if(ind >= 0){
			this.events[ind].timeToWarn = timeToWarn; 
			this.warnTimerCounter +=1;
			this.events[ind].warnId = this.warnTimerCounter;
			this.events[ind].warnFunc = callbackFunc;
			var timeLeft = this.events[ind].date - timeToWarn;
			//setEventTimer(time, callback)
			super.setEventTimer(timeLeft, callbackFunc)
		}

	}

}





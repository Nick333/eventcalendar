(function(calendar){
	var object = Object.create(calendar);
	object.timerCounter = 0;
	object.warnTimerCounter = 0;
	object.intervalCounter = 0;
	object.weeklyCounter = 0;


	object.addRepeatEvent = function(evName, dateTime, callback){
		const oneDayInMs = 24*3600*1000;
		const daysInWeek = 7;
		this.timerCounter = this.timerCounter + this.warnTimerCounter + this.weeklyCounter + 1;
		var now = new Date();
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		if(ind < 0){
			var selectedDate = new Date(dateTime).getTime();
			ind = this.events.length;
			var ev = {
				name: evName,
				date: selectedDate,
				timerId: 0,
				evFunction: callback.toString()
			}
			this.events.push(ev);
			console.log(ev);
			this.timerCounter +=1;
			this.events[ind].timerId = this.timerCounter;
			var dayNumber = new Date(this.events[ind].date).getDay();
			var timeToFirstCall;
			var today = new Date().getDay();
			var daysDifference = today - dayNumber;
			if(daysDifference >= 0){
				timeToFirstCall = daysDifference*oneDayInMs;
			}else{
				timeToFirstCall = (daysInWeek - daysDifference)*oneDayInMs;
			}
			var callbackFunc = callback.toString();
			this.events[ind].repeatDay = dayNumber;
			this.events[ind].repeatFunc = callback.toString();
			this.setEventInterval(timeToFirstCall, callbackFunc);
			this.intervalCounter += 1;
			this.weeklyCounter = this.timerCounter + this.warnTimerCounter + this.weeklyCounter + 1;
			this.events[ind].intervaId = this.intervalCounter;
			this.events[ind].weekTimerId = this.weeklyCounter;
			this.addToStorage();
		}else{
			console.log(`${evName} уже существует`);
		}

	}

	object.getExtendedEventsFromStorage = function(){
		const oneDayInMs = 24*3600*1000;
		const daysInWeek = 7;
		var now = new Date();
		var startIndex = 0;
		var today = now.getDay();
		var timeLeft;
		var daysDifference;
		var timeToFirstCall;
		if(window.localStorage.getItem('events') != undefined){
			this.events = JSON.parse(window.localStorage.getItem('events'));
			for(let i = 0; i < this.events.length; i++){
				if(!this.events[i].hasOwnProperty('repeatDay')){
					timeLeft = this.events[i].date - now;
					this.timerCounter +=1;
					this.events[i].timerId = this.timerCounter;
					this.setEventTimer(timeLeft, this.events[i].evFunction, this.events[i].name);
					continue;
				}else{
					daysDifference = today - new Date(this.events[i].date).getDay();
					if(daysDifference >= 0){
						timeToFirstCall = daysDifference*oneDayInMs;
					}else{
						timeToFirstCall = (daysInWeek - daysDifference)*oneDayInMs;
					}
					this.intervalCounter += 1;
					this.weeklyCounter = this.timerCounter + this.warnTimerCounter + this.weeklyCounter + 1;
					this.events[i].intervaId = this.intervalCounter;
					this.events[i].weekTimerId = this.weeklyCounter ;
					this.setEventInterval(timeToFirstCall, this.events[i].repeatFunc);
				}
			}
			for(let i = 0; i < this.events.length; i++){
				if(this.events[i].hasOwnProperty('timeBeforeEvent')){
					var warnTime = new Date(this.events[i].date - this.events[i].timeBeforeEvent).getTime();

						this.warnTimerCounter = this.timerCounter + this.warnTimerCounter + this.weeklyCounter + 1;;
						this.events[i].warnId = this.warnTimerCounter;
						this.setEventWarn(this.events[i].name, this.events[i].timeBeforeEvent, this.events[i].warnFunc);

				}
					
			}
		}
	}


	object.setEventInterval = function(timeToFirstEvCall, callback){
		const weekInMs = 604800000;
		setTimeout(()=>{
			eval('(' + callback + ')()');
		}, timeToFirstEvCall);
		setInterval(()=>{
			eval('(' + callback + ')()');
		}, weekInMs)
	}

	object.setEventWarnTimer = function(time, callback, evName){
		setTimeout(()=>{
			eval('(' + callback + ')()');
		}, time);
	}

	object.setEventWarn = function(evName, timeBeforeEvent, callback = null){
		var now = new Date().getTime();
		var ind = this.events.findIndex((el, index, arr)=>{
			return el.name == evName;
		});
		var callbackFunc = callback.toString() || this.commonFunction.toString();
		if(ind >= 0){
			this.events[ind].timeBeforeEvent = timeBeforeEvent; 
			this.warnTimerCounter = this.weeklyCounter + this.timerCounter + this.warnTimerCounter + 1;
			this.events[ind].warnId = this.warnTimerCounter;
			this.events[ind].warnFunc = callbackFunc;
			var timeLeft = this.events[ind].date - now - this.events[ind].timeBeforeEvent;
			this.setEventWarnTimer(timeLeft, callbackFunc, this.events[ind].name);
			this.addToStorage();
		}

	} 
	object.getExtendedEventsFromStorage();
	window.EventCalendar = object;

}(window.EventCalendar));
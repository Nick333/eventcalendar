(function(window){
	
	function EventCalendarConstructor(){
		this.events = [];
		this.timerCounter = 0;
	}
		EventCalendarConstructor.prototype.getEventsFromStorage = function(){
			var now = new Date();
			var startIndex = -1;
			if(window.localStorage.getItem('events') != undefined){
				this.events = JSON.parse(window.localStorage.getItem('events'));
					for(let i = 0; i<this.events.length; i++){
						let timeLeft = this.events[i].date - now;
						this.timerCounter +=1;
						this.events[i].timerId = this.timerCounter;
						this.setEventTimer(timeLeft, this.events[i].evFunction, this.events[i].name);
					}
			}
		}

		EventCalendarConstructor.prototype.addToStorage = function(){
			function compareDate(objA, objB){
				return objA.date-objB.date;
			}
			this.events.sort(compareDate);
			window.localStorage.setItem('events', JSON.stringify(this.events));
		}

		EventCalendarConstructor.prototype.getAllEvents = function(){
			return this.events;
		}

		EventCalendarConstructor.prototype.addEvent = function(evName, dateTime, callback){
			var now = new Date();
			var timeLeft;
			var ind = -1;
			var ind = this.events.findIndex((el, index, arr)=>{
				return el.name == evName;
			});
			if(ind < 0){
				var selectedDate = new Date(dateTime).getTime();
				var ind = this.events.length;
				var ev = {
					name: evName,
					date: selectedDate,
					timerId: 0,
					evFunction: callback.toString()
				}
				this.events.push(ev);
				console.log(ev);
				timeLeft = this.events[ind].date - now;
				this.timerCounter +=1;
				this.events[ind].timerId = this.timerCounter;
				this.setEventTimer(timeLeft, this.events[ind].evFunction, this.events[ind].name);
				this.addToStorage();
			}else{
				console.log(`${evName} уже существует`);
			}

		}

		EventCalendarConstructor.prototype.setEventTimer = function(time, callback, evName){
			setTimeout(()=>{
				eval('(' + callback + ')()');
				this.removeEvent(evName);
			}, time);
			
		}

		EventCalendarConstructor.prototype.removeEvent = function(evName){
			var ind = this.events.findIndex((el, index, arr)=>{
				return el.name == evName;
			});
			if(ind>=0){
				if('intervaId' in this.events[ind]){
					clearInterval(this.events[ind].intervaId);
					clearTimeout(this.events[ind].weekTimerId);
				}
				if('warnId' in this.events[ind]){
					clearTimeout(this.events[ind].warnId);
				}
				clearTimeout(this.events[ind].timerId);
				(ind>=0)&&this.events.splice(ind, 1);
				this.addToStorage();
			}else{
				console.log('no matches');
			}
			
			
		}

		EventCalendarConstructor.prototype.changeEvent = function(evName, newName = null, newDate = null, newCallback = null){
			var ind = this.events.findIndex((el, index, arr)=>{
				return el.name == evName;
			});
			var now = new Date();
			this.events[ind].name = (newName != null) ? newName : this.events[ind].name;
			this.events[ind].date = (newDate != null) ? new Date(newDate).getTime() : this.events[ind].date;
			this.events[ind].evFunction = (newCallback != null) ? newCallback.toString() : this.events[ind].evFunction;
			clearTimeout(this.events[ind].timerId);
			if(this.events[ind].date >= now){
				this.timerCounter +=1;
				this.events[ind].timerId = this.timerCounter;
				var timeLeft = this.events[ind].date - now;
				this.setEventTimer(timeLeft, this.events[ind].evFunction);
			}else{
				this.events[ind].timerId = 0;
			}
			this.addToStorage();
		}

		EventCalendarConstructor.prototype.getWeek = function(date) {  
			const daysQuantity = 7;
			const thursdayNumber = 4; 
			const weekInMs = 604800000;
		    var target = date;  
		    var dayNr  = (date.getDay() + 6) % daysQuantity;  
		    target.setDate(target.getDate() - dayNr + 3);  
		    var firstThursday = target.valueOf();  
		    target.setMonth(0, 1);  
		    if (target.getDay() != thursdayNumber) {  
		        target.setMonth(0, 1 + ((thursdayNumber - target.getDay()) + daysQuantity) % daysQuantity);  
		    }  
		    return 1 + Math.ceil((firstThursday - target) / weekInMs);
		}

		EventCalendarConstructor.prototype.getEventsByDate = function(year, month, day){
				var evs = [];
				var endOfDate = 24*3600*1000;
				var sdate = new Date(year, month, day).getTime();
				evs = this.events.filter(ev=>{
					let evDate = ev.date;
					if((evDate >= sdate) && (evDate < (sdate+endOfDate))){
						return ev;
					}
				});
			return evs;
		}

		EventCalendarConstructor.prototype.getEventsByWeekNumber = function(weekNumber){
		 	var evs = [];
		 	evs  = this.events.filter(ev=>{
		 		let evDate = new Date(ev.date);
		 		let evWeek = this.getWeek(evDate);
		 		if(evWeek == weekNumber){
		 			return ev;
		 		}
		 	});
		 	return evs;
		 }

		EventCalendarConstructor.prototype.getEventsByMonthNumber = function(year, month){
				var evs = [];
				evs = this.events.filter(ev=>{
				let evDate = new Date(ev.date).getMonth();
				let reqMon = new Date(year, month).getMonth();
				if(evDate == month){
					return ev;
				}
			});
			return evs;
		}

		EventCalendarConstructor.prototype.getEventsByInterval = function(startYear, startMonth, startDay, endYear, endMonth, endDay){
			var evs = [];
			var endOfDate = 24*3600*1000;
			var startDate = new Date(startYear, startMonth, startDay).getTime();
			var endDate = new Date(endYear, endMonth, endDay).getTime();
			evs = this.events.filter(ev=>{
				let evDate = ev.date;
				if((evDate >= startDate) && (evDate < (endDate+endOfDate))){
					return ev;
					}
			});
			return evs;
		}
	window.EventCalendar = new EventCalendarConstructor();
		
	

}(window));


